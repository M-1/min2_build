set fail=%cd%\fail.bat

cd openssl
perl Configure VC-WIN32 no-asm no-shared no-ssl2 no-ssl3 no-idea --prefix="." || call %fail%
call ms\do_ms || call %fail%
nmake -f ms\nt.mak || call %fail%
nmake -f ms\nt.mak install || call %fail%
cd ..