#!/bin/sh

# stop in case of an error
set -e

. ./_build_openssl.sh
. ./_build_udt.sh
. ./_build_sqlitecpp.sh
. ./_build_json_spirit.sh

. ./_build_min2.sh
