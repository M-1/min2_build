set fail=%cd%\fail.bat

cd min2
if not exist build mkdir build || call %fail%
cd build

rem !!! change "v120_xp" to something else if you don't want to target WinXP with MSVC2013 !!!
cmake -DBOOST_ROOT:PATH=../boost -T v120_xp .. || call %fail%

msbuild minet.sln /p:Configuration="Release" || call %fail%
cd ..
cd ..
