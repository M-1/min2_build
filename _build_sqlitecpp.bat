cd sqlitecpp
if not exist build mkdir build
cd build
cmake .. || call %fail%
cd sqlite3
msbuild sqlite.vcxproj /p:Configuration="Release" || call %fail%
cd ..
msbuild SQLiteCpp.vcxproj /p:Configuration="Release" || call %fail%
cd ..
cd ..
