cd openssl
./Configure darwin64-x86_64-cc no-shared no-ssl2 no-ssl3 no-idea no-dso

# remove dependency on libdl (OpenSSL's build system cannot do it automatically when disabling dso)
sed 's/^\(EX_LIBS=\)\(.*\)-ldl\(.*\)$/\1\2\3/' Makefile > tmp
mv tmp Makefile

make depend
make
# TODO: make install
cd ..
