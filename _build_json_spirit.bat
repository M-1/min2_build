set fail=%cd%\fail.bat

cd json_spirit/json_spirit
if not exist build mkdir build || call %fail%
cd build
cmake -DBOOST_ROOT:PATH=../../boost .. || call %fail%
msbuild json_spirit.vcxproj /p:Configuration="Release" || call %fail%
cd ..
cd ..
cd ..
